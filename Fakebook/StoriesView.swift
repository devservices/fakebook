//
//  StoriesView.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct StoriesView : View {
    var stories: [Story]
    
    var body: some View {
        ScrollView(isScrollEnabled: true, alwaysBounceHorizontal: true, alwaysBounceVertical: false, showsHorizontalIndicator: false) {
            HStack(spacing: 10) {
                ForEach(stories) { story in
                    CardView(story: story)
                }
            }
            .padding([.leading, .trailing], 8)
        }
    }
}

#if DEBUG
struct StoriesView_Previews : PreviewProvider {
    static var previews: some View {
        StoriesView(stories: HomePageModel().stories)
    }
}
#endif
