//
//  HomePage.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct HomePage : View {
    @State var model = HomePageModel()
    
    var body: some View {
        NavigationView {
            List {
                StoriesView(stories: model.stories)
                    .frame(height: 200)
                    .listRowInsets(EdgeInsets())
                
                ForEach(model.feeds.identified(by: \.self)) { feed in
                    FeedView(feed: feed)
                        .frame(height: 550)
                    }
                    .listRowInsets(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
                }
                .navigationBarTitle(Text("Fakebook"))
        }
    }
}

#if DEBUG
struct HomePage_Previews : PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
#endif
