//
//  Model.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import Foundation
import SwiftUI

struct Story: Hashable, Identifiable {
    var imageName: String
    var user: User
    var id = UUID()
}

struct Feed: Hashable {
    var imageName: String
    var text: String
    var user: User
}

struct User: Hashable {
    var userImageName: String
    var userName: String
}

struct HomePageModel {
    var stories: [Story] = {
        var user = User(userImageName: "profile", userName: "Cristian")
        return [Story(imageName: "story1", user: user),
                Story(imageName: "story2", user: user),
                Story(imageName: "story3", user: user),
                Story(imageName: "story4", user: user),
                Story(imageName: "story5", user: user)]
    }()
    
    var feeds: [Feed] = {
        var user = User(userImageName: "profile", userName: "Cristian")
        return [Feed(imageName: "feed1", text: "Surfing Safari", user: user),
                Feed(imageName: "feed2", text: "Maybe it's Venice?", user: user),
                Feed(imageName: "feed4", text: "We can all be designers", user: user),
                Feed(imageName: "feed3", text: "Space Cowboy", user: user)]
    }()
}
