//
//  ProfileView.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct ProfileView : View {
    var imageName: String
    
    var body: some View {
        Image(imageName)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 50, height: 50)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.blue, lineWidth: 5))
            .shadow(radius: 4)
    }
}

#if DEBUG
struct ProfileView_Previews : PreviewProvider {
    static var previews: some View {
        ProfileView(imageName: "profile")
            .previewLayout(.fixed(width: 100, height: 100))
    }
}
#endif
