//
//  FeedView.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct FeedView : View {
    var feed: Feed
    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: 16) {
                ProfileView(imageName: feed.user.userImageName)
                VStack(alignment: .leading) {
                    HStack(spacing: 4) {
                        Text(feed.user.userName)
                            .font(.headline)
                        Text("shared a")
                        Text("post")
                            .font(.headline)
                    }
                    Text("Published yesterday")
                        .font(.caption)
                        .color(.gray)
                }
                Spacer()
                Image(systemName: "ellipsis")
            }
            .padding()
            Text(feed.text)
                .padding([.leading, .trailing])
            Image(feed.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipped()
        }
    }
}

#if DEBUG
struct FeedView_Previews : PreviewProvider {
    static var previews: some View {
        FeedView(feed: HomePageModel().feeds[0])
    }
}
#endif
