//
//  BackgroundView.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct BackgroundView : View {
    var imageName: String
    var body: some View {
        ZStack {
            Image(imageName)
                .resizable()
                .aspectRatio(1, contentMode: .fill)
                .clipped()
            Rectangle()
                .foregroundColor(.clear)
                .background(LinearGradient(
                    gradient: Gradient(colors: [Color.gray.opacity(0), Color.black.opacity(0.9)]),
                    startPoint: .center,
                    endPoint: .bottom),
                            cornerRadius: 0)
        }
    }
}

#if DEBUG
struct BackgroundView_Previews : PreviewProvider {
    static var previews: some View {
        BackgroundView(imageName: "story1")
    }
}
#endif
