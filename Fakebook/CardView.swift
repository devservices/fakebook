//
//  CardView.swift
//  Fakebook
//
//  Created by Cristian Pena on 21/06/2019.
//  Copyright © 2019 Cristian Pena. All rights reserved.
//

import SwiftUI

struct CardView : View {
    var story: Story
    
    var body: some View {
        ZStack {
            BackgroundView(imageName: story.imageName)
            HStack {
                VStack(alignment: .leading) {
                    ProfileView(imageName: story.user.userImageName)
                    Spacer()
                    Text(story.user.userName)
                        .font(.system(size: 22))
                        .color(.white)
                }
                .padding()
                Spacer()
        }
                .frame(width:130)
        }
            .frame(width:130, height: 200)
            .cornerRadius(20)
            .clipped()
    }
}

#if DEBUG
struct CardView_Previews : PreviewProvider {
    static var previews: some View {
        CardView(story: HomePageModel().stories[0])
            .previewLayout(.fixed(width: 300, height: 500))
    }
}
#endif
